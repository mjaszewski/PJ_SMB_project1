package pl.pjatk.mj.smb.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

import java.util.List;

/**
 * An endpoint class we are exposing
 */
@Api(
        name = "itemApi",
        version = "v1",
        resource = "item",
        namespace = @ApiNamespace(
                ownerDomain = "backend.smb.mj.pjatk.pl",
                ownerName = "backend.smb.mj.pjatk.pl"
        )
)
public class ItemEndpoint {
    private ItemRepository itemRepository = new ItemRepository();

    @ApiMethod(name = "getItem")
    public Item getItem(@Named("id") Long id) {
        return itemRepository.findOne(id);
    }

    @ApiMethod(name = "getAllItems")
    public List<Item> getAllItems() {
        return itemRepository.findAll();
    }

    @ApiMethod(name = "insertItem")
    public Item insertItem(Item item) {
        itemRepository.save(item);
        return item;
    }

    @ApiMethod(name = "deleteItem")
    public void deleteItem(@Named("id") Long id) {
        itemRepository.delete(id);
    }
}