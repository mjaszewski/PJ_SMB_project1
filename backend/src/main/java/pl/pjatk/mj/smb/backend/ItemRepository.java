package pl.pjatk.mj.smb.backend;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;
import static com.googlecode.objectify.ObjectifyService.register;


/**
 * Created by Mateusz on 12.12.2016.
 */

public class ItemRepository {

    static {
        register(Item.class);
    }

    public List<Item> findAll() {
        return ofy().load().type(Item.class).list();
    }

    public Item findOne(Long id) {
         return ofy().load().type(Item.class).id(id).now();
    }

    public void save(Item item) {
        ofy().save().entity(item).now();
    }

    public void delete(Long id) {
        ofy().delete().type(Item.class).id(id).now();
    }
}
