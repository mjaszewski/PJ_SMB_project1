package pl.pjatk.mj.smb.project1.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.ExecutionException;

import pl.pjatk.mj.smb.backend.itemApi.model.Item;
import pl.pjatk.mj.smb.project1.R;
import pl.pjatk.mj.smb.project1.api.ItemApiClient;
import pl.pjatk.mj.smb.project1.validation.ItemFormValidator;

import static pl.pjatk.mj.smb.project1.activity.ListActivity.ITEM_ID;

public class EditItemActivity extends AddItemActivity {

    private Button saveButton;
    private EditText nameEditText;
    private EditText numberEditText;
    private ItemApiClient itemApiClient;
    private Item item;
    private ItemFormValidator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        itemApiClient = new ItemApiClient();

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        numberEditText = (EditText) findViewById(R.id.numberEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        validator = new ItemFormValidator(this, nameEditText, numberEditText);

        setupNameTextWatcher();
        setupNumberTextWatcher();
        setupUpdateAction();

        loadItem();
    }

    private void loadItem() {
        Long itemId = getIntent().getExtras().getLong(ITEM_ID);
        try {
            item = itemApiClient.findOne(itemId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        nameEditText.setText(item.getName());
        numberEditText.setText(item.getNumber().toString());
    }

    private void setupNameTextWatcher() {
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validator.validateName();
            }
        });
    }

    private void setupNumberTextWatcher() {
        numberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validator.validateNumber();
            }
        });
    }

    private void setupUpdateAction() {
        saveButton.setOnClickListener(v -> {
            boolean isNameValid = validator.validateName();
            boolean isNumberValid = validator.validateNumber();
            if (isNameValid && isNumberValid) {
                item.setName(nameEditText.getText().toString());
                item.setNumber(Integer.parseInt(numberEditText.getText().toString()));
                try {
                    itemApiClient.update(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            }
        });
    }
}
