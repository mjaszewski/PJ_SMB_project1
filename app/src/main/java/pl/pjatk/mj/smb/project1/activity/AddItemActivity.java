package pl.pjatk.mj.smb.project1.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import pl.pjatk.mj.smb.backend.itemApi.model.Item;
import pl.pjatk.mj.smb.project1.R;
import pl.pjatk.mj.smb.project1.api.ItemApiClient;
import pl.pjatk.mj.smb.project1.validation.ItemFormValidator;

public class AddItemActivity extends Activity {

    private Button saveButton;
    private EditText nameEditText;
    private EditText numberEditText;

    private ItemFormValidator validator;
    private ItemApiClient itemApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        itemApiClient = new ItemApiClient();

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        numberEditText = (EditText) findViewById(R.id.numberEditText);
        saveButton = (Button) findViewById(R.id.saveButton);

        validator = new ItemFormValidator(this, nameEditText, numberEditText);

        setupNameTextWatcher();
        setupNumberTextWatcher();
        setupSaveAction();
    }

    private void setupNameTextWatcher() {
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validator.validateName();
            }
        });
    }

    private void setupNumberTextWatcher() {
        numberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                validator.validateNumber();
            }
        });
    }

    private void setupSaveAction() {
        saveButton.setOnClickListener(v -> {
            boolean isNameValid = validator.validateName();
            boolean isNumberValid = validator.validateNumber();
            if (isNameValid && isNumberValid) {
                String name = nameEditText.getText().toString();
                int number = Integer.parseInt(numberEditText.getText().toString());
                Item item = new Item();
                item.setName(name);
                item.setNumber(number);
                try {
                    itemApiClient.save(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            }
        });
    }
}
