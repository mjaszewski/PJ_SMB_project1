package pl.pjatk.mj.smb.project1.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import pl.pjatk.mj.smb.project1.R;
import pl.pjatk.mj.smb.project1.model.Color;

import static java.util.Arrays.*;

public class SettingsActivity extends Activity {

    public static final String FONT_SIZE = "fontSize";
    public static final String FONT_COLOR = "fontColor";
    private static final List<Color> FONT_COLORS = asList(
            new Color("Czarny", "#000000"),
            new Color("Czerwony", "#FF0000"),
            new Color("Zielony", "#00FF00"),
            new Color("Niebieski", "#0000FF"));

    public static final int DEFAULT_FONT_COLOR = FONT_COLORS.get(0).getColor();
    public static final String SHARED_PREFERENCES_NAME = "settings";

    private List<Integer> fontSizes;
    private Spinner fontSizeSpinner;
    private Spinner fontColorSpinner;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();

        initFontSizes();

        fontSizeSpinner = (Spinner) findViewById(R.id.fontSizeSpinner);
        fontSizeSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, fontSizes));
        fontSizeSpinner.setSelection(indexOfFontSize(sharedPreferences.getInt(FONT_SIZE, getResources().getInteger(R.integer.default_font_size))));

        fontColorSpinner = (Spinner) findViewById(R.id.fontColorSpinner);
        fontColorSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, FONT_COLORS));
        fontColorSpinner.setSelection(indexOfFontColor(sharedPreferences.getInt(FONT_COLOR, DEFAULT_FONT_COLOR)));

        setupFontSizeSelectionListener();
        setupFontColorSelectionListener();
    }

    private void setupFontSizeSelectionListener() {
        fontSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int fontSize = (int) parent.getItemAtPosition(position);
                sharedPreferencesEditor.putInt(FONT_SIZE, fontSize);
                sharedPreferencesEditor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void setupFontColorSelectionListener() {
        fontColorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int fontColor = ((Color) parent.getItemAtPosition(position)).getColor();
                sharedPreferencesEditor.putInt(FONT_COLOR, fontColor);
                sharedPreferencesEditor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }
    
    private void initFontSizes() {
        int minFontSize = getResources().getInteger(R.integer.min_font_size);
        int maxFontSize = getResources().getInteger(R.integer.max_font_size);
        fontSizes = new ArrayList<>();
        for (int i = minFontSize; i < maxFontSize; i++) {
            fontSizes.add(i);
        }
    }

    private int indexOfFontSize(int fontSize) {
        int index = fontSizes.indexOf(fontSize);
        return index != -1 ? index : 0;
    }

    private int indexOfFontColor(int fontColor) {
        for (int i = 0; i < FONT_COLORS.size(); i++) {
            if (FONT_COLORS.get(i).getColor() == fontColor) {
                return i;
            }
        }
        return 0;
    }

}
