package pl.pjatk.mj.smb.project1.adpter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import pl.pjatk.mj.smb.backend.itemApi.model.Item;
import pl.pjatk.mj.smb.project1.R;
import pl.pjatk.mj.smb.project1.api.ItemApiClient;

import static android.util.TypedValue.COMPLEX_UNIT_PT;

/**
 * Created by Mateusz on 03.11.2016.
 */
public class ItemAdapter extends ArrayAdapter<Item> {

    private static final String PREFIX = " x ";
    private ItemApiClient itemApiClient = new ItemApiClient();
    private int fontColor;
    private int fontSize;

    public ItemAdapter(Context context, List<Item> items, int fontColor, int fontSize) {
        super(context, 0, items);
        this.fontColor = fontColor;
        this.fontSize = fontSize;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_layout, parent, false);
        }
        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
        checkBox.setChecked(item.getStatus());
        checkBox.setText(item.getName());
        checkBox.setOnClickListener(buttonView -> {
            item.setStatus(checkBox.isChecked());
            try {
                itemApiClient.update(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        checkBox.setTextColor(fontColor);
        checkBox.setTextSize(COMPLEX_UNIT_PT, fontSize);

        TextView countTextView = (TextView) convertView.findViewById(R.id.countTextView);
        countTextView.setText(PREFIX + Integer.toString(item.getNumber()));
        countTextView.setTextColor(fontColor);
        countTextView.setTextSize(COMPLEX_UNIT_PT, fontSize);

        return convertView;
    }

}
