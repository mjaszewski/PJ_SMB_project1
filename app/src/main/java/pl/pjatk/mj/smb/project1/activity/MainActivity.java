package pl.pjatk.mj.smb.project1.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import pl.pjatk.mj.smb.project1.R;

public class MainActivity extends Activity {

    private Button openSettingsButton;
    private Button openListButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openSettingsButton = (Button) findViewById(R.id.openSettingsButton);
        openListButton = (Button) findViewById(R.id.openListButton);
        openListButton.setOnClickListener(e -> startActivity(new Intent(this, ListActivity.class)));
        openSettingsButton.setOnClickListener(e -> startActivity(new Intent(this, SettingsActivity.class)));
    }
}
