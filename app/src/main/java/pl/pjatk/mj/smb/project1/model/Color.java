package pl.pjatk.mj.smb.project1.model;

import static android.graphics.Color.*;

/**
 * Created by Mateusz on 01.11.2016.
 */
public class Color {

    private final String name;
    private final int color;

    public Color(String name, String colorString) {
        this.name = name;
        this.color = parseColor(colorString);
    }

    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }

    @Override
    public String toString() {
        return name;
    }
}
