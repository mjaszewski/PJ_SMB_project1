package pl.pjatk.mj.smb.project1.validation;

import android.content.Context;
import android.widget.EditText;

import pl.pjatk.mj.smb.project1.R;

/**
 * Created by Mateusz on 04.11.2016.
 */
public class ItemFormValidator {

    private EditText nameEditText;
    private EditText numberEditText;
    private Context context;

    public ItemFormValidator(Context context, EditText nameEditText, EditText numberEditText) {
        this.nameEditText = nameEditText;
        this.numberEditText = numberEditText;
        this.context = context;
    }

    public boolean validateName() {
        if (nameEditText.getText().length() == 0) {
            nameEditText.setError(context.getString(R.string.name_is_required));
            return false;
        }
        return true;
    }

    public boolean validateNumber() {
        if (numberEditText.getText().length() == 0) {
            numberEditText.setError(context.getString(R.string.number_is_required));
            return false;
        } else if (Integer.parseInt(numberEditText.getText().toString()) <= 0) {
            numberEditText.setError(context.getString(R.string.number_must_be_greater_than_zero));
            return false;
        }
        return true;
    }
}
