package pl.pjatk.mj.smb.project1.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;

import java.util.List;

import pl.pjatk.mj.smb.backend.itemApi.model.Item;
import pl.pjatk.mj.smb.project1.R;
import pl.pjatk.mj.smb.project1.adpter.ItemAdapter;
import pl.pjatk.mj.smb.project1.api.ItemApiClient;

import static pl.pjatk.mj.smb.project1.activity.SettingsActivity.*;
import static pl.pjatk.mj.smb.project1.activity.SettingsActivity.FONT_COLOR;
import static pl.pjatk.mj.smb.project1.activity.SettingsActivity.FONT_SIZE;

public class ListActivity extends Activity {

    public static final String ITEM_ID = "itemId";

    private Button addItemButton;
    private ItemApiClient itemApiClient;
    private ListView listView;
    private List<Item> items;
    private ItemAdapter itemAdapter;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        itemApiClient = new ItemApiClient();

        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);

        addItemButton = (Button) findViewById(R.id.addItemButton);
        addItemButton.setOnClickListener(v -> startActivity(new Intent(this, AddItemActivity.class)));

        listView = (ListView) findViewById(R.id.listView);
        setupPopupMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadItems();
    }

    private void setupPopupMenu() {
        listView.setOnItemLongClickListener((parent, view, position, id) -> {
            PopupMenu popup = new PopupMenu(this, view);
            popup.getMenuInflater().inflate(R.menu.item_popup_menu, popup.getMenu());
            popup.setOnMenuItemClickListener(menuItem -> {
                if (menuItem.getItemId() == R.id.editItem) {
                    Intent intent = new Intent(this, EditItemActivity.class);
                    intent.putExtra(ITEM_ID, itemAdapter.getItem(position).getId());
                    startActivity(intent);
                }
                if (menuItem.getItemId() == R.id.deleteItem) {
                    Item item = itemAdapter.getItem(position);
                    try {
                        itemApiClient.delete(item.getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    items.remove(item);
                    itemAdapter.notifyDataSetChanged();
                }
                return true;
            });
            popup.show();
            return true;
        });
    }

    private void loadItems() {
        int fontSize = sharedPreferences.getInt(FONT_SIZE, getResources().getInteger(R.integer.default_font_size));
        int fontColor = sharedPreferences.getInt(FONT_COLOR, DEFAULT_FONT_COLOR);
        try {
            items = itemApiClient.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        itemAdapter = new ItemAdapter(this, items, fontColor, fontSize);
        listView.setAdapter(itemAdapter);
    }
}
