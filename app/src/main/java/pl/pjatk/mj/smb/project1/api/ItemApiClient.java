package pl.pjatk.mj.smb.project1.api;


import android.os.AsyncTask;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import pl.pjatk.mj.smb.backend.itemApi.ItemApi;
import pl.pjatk.mj.smb.backend.itemApi.model.Item;

/**
 * Created by Mateusz on 03.11.2016.
 */
public class ItemApiClient {

    private final ItemApi api;

    public ItemApiClient() {
        ItemApi.Builder itemApiBuilder = new ItemApi.Builder(AndroidHttp.newCompatibleTransport(), new JacksonFactory(), null);
        itemApiBuilder.setRootUrl("http://10.0.2.2:8080/_ah/api");
        api = itemApiBuilder.build();
    }

    public Item findOne(Long id) throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, Item>() {
            @Override
            protected Item doInBackground(Void... params) {
                try {
                    return api.getItem(id).execute();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }.execute().get();
    }

    public List<Item> findAll() throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, List<Item>>() {
            @Override
            protected List<Item> doInBackground(Void... params) {
                try {
                    return api.getAllItems().execute().getItems();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }.execute().get();
    }

    public void save(Item item) throws ExecutionException, InterruptedException {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    api.insertItem(item).execute();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return null;
            }
        }.execute().get();
    }

    public void update(Item item) throws ExecutionException, InterruptedException {
        save(item);
    }

    public void delete(Long id) throws ExecutionException, InterruptedException {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    api.deleteItem(id).execute();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return null;
            }
        }.execute().get();
    }
}
